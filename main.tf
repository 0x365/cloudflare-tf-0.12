terraform {
  required_version = ">= 0.12"
}

provider "cloudflare" {
  version = "~> 2.0"
  email   = var.cloudflare_email
  api_key = var.cloudflare_api_key
}

data "cloudflare_zones" "main_zone" {
  filter {
    name   = var.domain
    status = "active"
    paused = false
  }
}

resource "cloudflare_zone_settings_override" "domain-settings" {
  zone_id = "${lookup(data.cloudflare_zones.main_zone.zones[0], "id")}"

  settings {
    tls_1_3                  = var.tls_1_3
    min_tls_version          = var.min_tls_version
    automatic_https_rewrites = var.https_rewrites
    ssl                      = var.ssl
    waf                      = var.waf
  }
}

resource "cloudflare_record" "dns_normal_records" {
  count    = length(var.dns_records)
  zone_id  = "${lookup(data.cloudflare_zones.main_zone.zones[0], "id")}"
  #domain   = element(var.dns_records[count.index], 0)
  name     = element(var.dns_records[count.index], 1)
  value    = element(var.dns_records[count.index], 2)
  type     = element(var.dns_records[count.index], 3)
  proxied  = element(var.dns_records[count.index], 4)
  priority = element(var.dns_records[count.index], 5)
  ttl      = "1"
}